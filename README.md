# Linux server configuration project - FSND

## Useful info
IP address : 35.167.11.210

SSH port : 2200

Application URL : [http://ec2-35-167-11-210.us-west-2.compute.amazonaws.com](http://ec2-35-167-11-210.us-west-2.compute.amazonaws.com)

## Step by step walkthrough

### 1. Create new user named __grader__ and grant it sudo permission.

1. Login into remote vm as root using ssh `ssh -i ~/.ssh/udacity_key.rsa root@35.167.11.210`.
2. Add new user named grader : `sudo adduser grader`.
3. Create a new file named *grader* under sudoers : `sudo nano /etc/sudoers.d/grader`.
4. Add the following text `grader ALL=(ALL:ALL) ALL` in the newly created *grader* file.
5. In order to prevent the "sudo: unable to resolve host" error, edit the hosts :
    1. `sudo nano /etc/hosts`.
    2. Add the host: `127.0.1.1 ip-35-167-11-210`

### 2. Update currently installed packages.
1. `sudo apt-get update`
2. `sudo apt-get upgrade`

### 3. Configure local timezone to UTC
1. Set local timezone to UTC using : `sudo dpkg-reconfigure tzdata`.
2. Install *ntp daemon ntpd* for a better synchronization of the server's time over the network connection: `sudo apt-get install ntp`.

Source: [UbuntuTime](https://help.ubuntu.com/community/UbuntuTime).
### 4. Configure key-based authentication for *grader* user.
1. Generate an encryption key on your local system using `ssh-keygen`. [I gave the name **graderkey** to my key]
2. Now login the remote vm as root and create the following file : `touch /home/grader/.ssh/authorized_keys`.
3. Copy the content of the *graderkey.pub* file from your local machine to the */home/grader/.ssh/authorized_keys* file you just created on the remote VM. Then change some permissions:
	1. `sudo chmod 700 /home/grader/.ssh`.
	2. `sudo chmod 644 /home/grader/.ssh/authorized_keys`.
	3. Finally change the owner from *root* to *grader*: `$ sudo chown -R grader:grader /home/grader/.ssh`.
4. Now you are able to log into the remote VM through ssh with the following command: `$ ssh -i ~/.ssh/udacity_key.rsa grader@35.167.11.210`.

### 5. Enforcing key-based ssh authentication
1. Type the following command `sudo nano /etc/ssh/sshd_config` in terminal. Find the *PasswordAuthentication* line and edit it to *no*.
2. `sudo service ssh restart`.

### 6. Change the SSH port to  2200
1. Open this `sudo nano /etc/ssh/sshd_config` file and find the *Port* line and edit it to *2200*.
2. Now restart the ssh service using command : `sudo service ssh restart`.
3. Now you are able to log into the remote VM through ssh with the following command: `$ ssh -i ~/.ssh/graderkey -p 2200 grader@35.167.11.210`.

### 7. Disable ssh login for *root* user
1. Open the following `sudo nano /etc/ssh/sshd_config` file, find the *PermitRootLogin* line and edit it to *no*.
2. Now restart the service : `sudo service ssh restart`.

Source: [Askubuntu](http://askubuntu.com/questions/27559/how-do-i-disable-remote-ssh-login-as-root-from-a-server).

### 8. Configure the UFW (Ubuntu firewall)
1. `sudo ufw allow 2200/tcp`.
2. `sudo ufw allow 80/tcp`.
3. `sudo ufw allow 123/udp`.
4. `sudo ufw enable`.

### 9. Configure cron scripts to automatically manage package updates

1. Install *unattended-upgrades* if not already installed: `sudo apt-get install unattended-upgrades`.
2. To enable it, do: `sudo dpkg-reconfigure --priority=low unattended-upgrades`.

### 10. Install Git

1. `sudo apt-get install git`.
2. Configure your username using : `git config --global user.name <username>`.
3. Configure your email using : `git config --global user.email <email>`.

### 11. Install Apache, mod_wsgi

1. `sudo apt-get install apache2`.
2. Install *mod_wsgi* with the following command: `sudo apt-get install libapache2-mod-wsgi python-dev`.
3. Enable *mod_wsgi* : `sudo a2enmod wsgi`.
4. Restart the service : `sudo service apache2 start`.

### 12. Clone the Item Catalog app from Github

1. `cd /var/www` , then make directory named *catalog* using `sudo mkdir catalog`.
2. Change owner for the *catalog* folder: `$ sudo chown -R grader:grader catalog`.
3. Move inside that newly created folder: `$ cd /catalog` and clone the catalog repository from Github: `$ git clone https://github.com/immabhay/item-catalog.git catalog`.
[The repo downloaded has the name item-catalog we need to rename it to catalog, type `mv /var/www/catalog/item-catalog /var/www/catalog/catalog` ]
4. Make the GitHub repository inaccessible:
  1. Create and open .htaccess file: `cd /var/www/catalog/` and `sudo nano .htaccess`
  2. Paste in the following:
```
RedirectMatch 404 /\.git
```
4. Make a *catalog.wsgi* file to serve the application over the *mod_wsgi*. That file should look like this:

```python
import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, "/var/www/catalog/")

from catalog import app as application
app.secret_key = 'Your_secret_key'
```

### 13. Install virtual environment, Flask and the project's dependencies

1. Install *pip*, the tool for installing Python packages : `sudo apt-get install python-pip`.
2. If *virtualenv* is not installed, use *pip* to install it using the following command: `sudo pip install virtualenv`.
3. Move to the *catalog* folder : `cd /var/www/catalog`. Then create a new virtual environment with the following command : `sudo virtualenv venv`.
4. Activate the virtual environment : `source venv/bin/activate`.
5. Change permissions to the virtual environment folder : `sudo chmod -R 777 venv`.
6. Install Flask by using : `pip install Flask`.
7. Install all the other project's dependencies : `pip install bleach httplib2 request oauth2client sqlalchemy python-psycopg2`.

### 14 - Configure and enable a new virtual host

1. Create a virtual host conifg file using : `sudo nano /etc/apache2/sites-available/catalog.conf`.
2. Paste in the following lines of code in **catalog.conf**:
```
<VirtualHost *:80>
    ServerName 35.167.11.210
    ServerAlias ec2-35-167-11-210.us-west-2.compute.amazonaws.com
    ServerAdmin admin@35.167.11.210
    WSGIDaemonProcess catalog python-path=/var/www/catalog:/var/www/catalog/venv/lib/python2.7/site-packages
    WSGIProcessGroup catalog
    WSGIScriptAlias / /var/www/catalog/catalog.wsgi
    <Directory /var/www/catalog/catalog/>
        Order allow,deny
        Allow from all
    </Directory>
    Alias /static /var/www/catalog/catalog/static
    <Directory /var/www/catalog/catalog/static/>
        Order allow,deny
        Allow from all
    </Directory>
    ErrorLog ${APACHE_LOG_DIR}/error.log
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
3. Enable the new virtual host: `sudo a2ensite catalog`.

Source: [DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-run-django-with-mod_wsgi-and-apache-with-a-virtualenv-python-environment-on-a-debian-vps).

### 15. Install and configure PostgreSQL

1. Install some necessary Python packages for working with PostgreSQL: `sudo apt-get install libpq-dev python-dev`.
2. Install PostgreSQL: `sudo apt-get install postgresql postgresql-contrib`.
3. Postgres is automatically creating a new user during its installation, whose name is 'postgres'. That is a tusted user who can access the database software. So let's change the user with: `sudo su - postgres`, then connect to the database system with `psql`.
4. Create a new user called 'catalog' with his password: `# CREATE USER catalog WITH PASSWORD 'yourpassword';`.
5. Give *catalog* user the CREATEDB capability: `# ALTER USER catalog CREATEDB;`.
6. Create the 'catalog' database owned by *catalog* user: `# CREATE DATABASE catalog WITH OWNER catalog;`.
7. Connect to the database: `# \c catalog`.
8. Revoke all rights: `# REVOKE ALL ON SCHEMA public FROM public;`.
9. Lock down the permissions to only let *catalog* role create tables: `# GRANT ALL ON SCHEMA public TO catalog;`.
10. Log out from PostgreSQL: `# \q`. Then return to the *grader* user: `exit`.
11. Inside the Flask application, the database connection is now performed with:
```python
engine = create_engine('postgresql://catalog:yourpassword@localhost/catalog')
```
12. Setup the database with: `/var/www/catalog/catalog/database_setup.py`.
13. Populate the database : `/var/www/catalog/catalog/lotsofmenus.py`.

### 16. Restart the apache to launch the app
1. Restart the apache using : `sudo service apache2 restart`.
